const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();

// Данные из бинес апи. Сотрудник, от имени которого создается сделка, организация и департамент
const employeeId = 1524850;
const organizationId = 75535;
const departmentId = 9;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Для проверки, что сервис поднялся
app.get('/healthcheck', (req, res) => {
  return res.status(200).send({
    message: 'OK'
  });
});

// Чтобы посмотреть что приходит в запросе
app.post('/writerequest', (req, res) => {
  fs.writeFileSync('request', JSON.stringify(req.body));
  return res.status(200).send({
    message: 'OK'
  });
});

// Получение запроса и его отправка на сервер бизнеса
const apihelper = require('./apihelper');
app.post('/api/request', async (req, res) => {
  return res.status(200).send({
    data: await apihelper.sendPost('deals', {
      name: req.body.Name || 'Клиент без имени',
      author_employee_id: employeeId,
      responsible_employee_id: employeeId,
      executor_employee_id: employeeId,
      owner_employee_id: employeeId,
      organization_id: organizationId,
      departments_ids: [departmentId],
      description: formatBody(req.body)
    })
  });
});

function formatBody(body) {
  let str = '';

  Object.keys(body).forEach(key => {
    str += key + ': ' + body[key] + '\r\n';
  });

  return str;
}

const PORT = 4000;

app.listen(PORT, () => {
  console.log(`server running on port ${PORT}`);
});
