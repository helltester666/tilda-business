const axios = require('axios');
const FormData = require('form-data');
const md5 = require('md5');
const fs = require('fs');
var urlencode = require('locutus/php/url/urlencode');
const qs = require('querystring');

// appId и секрет из бизнес апи
const appId = 683273;
const secret = 'wnsCDnszhlE1MzJZGbZkfLEPKiG4upnR';

// эндпоинт для запросов в бизнес апи
axios.defaults.baseURL = 'https://aziks_butik.class365.ru/api/rest';
// ОБЯЗАТЕЛЬНО! без этоого заголовка не будет работать
axios.defaults.headers.post['Content-Type'] =
  'application/x-www-form-urlencoded';

//Формализация запроса к апи. Подробнее в описании апи бизнеса
async function sendPost(model, params, retry = true) {
  let formData = sortParams(params);
  console.log(formData);

  params.app_id = appId;
  params = sortParams(params);

  console.log(params);

  let token = fs.readFileSync('token', 'utf8');

  let hash = md5(token + secret + params);

  return await axios({
    method: 'post',
    url: `/${model}.json?app_id=${appId}&app_psw=${hash}`,
    data: formData
  })
    .then(function(response) {
      fs.writeFileSync('token', response.data.token);
      return response;
    })
    .then(function(response) {
      return response.data;
    })
    .catch(async function(error) {
      if (retry && error.response.status == 401) {
        return await axios({
          method: 'get',
          url: `/repair.json?app_id=${appId}&app_psw=83c1d7168bc1104089ec0b0fcf96ce32`
        })
          .then(function(response) {
            fs.writeFileSync('token', response.data.token);
            sendPost(model, params, false);
          })
          .catch(error => {
            console.log(error);
          });
      }
      fs.appendFileSync('request', `${error}\r\n`);
      return error;
    });
}

function sortParams(params) {
  let ordered = {};
  Object.keys(params)
    .sort()
    .forEach(function(key) {
      ordered[key] = params[key];
    });

  let str = '';

  Object.keys(ordered).forEach(function(key) {
    if (Array.isArray(ordered[key])) {
      for (let i = 0; i < ordered[key].length; i++) {
        str += '&' + `${key}%5B${i}%5D=` + ordered[key][i];
      }
    } else {
      str += '&' + key + '=' + urlencode(ordered[key]);
    }
  });

  return str.slice(1);
}

module.exports = {
  sendPost
};
